input = File.read('./day10input.txt').lines.map(&:to_i).sort
input.unshift(0)
input.push(input.last + 3)

diffs = [0,0]
input.each_with_index do |i, ind|
  next if ind >= input.length - 1
  case input[ind + 1] - i
  when 1 then diffs[0] += 1
  when 3 then diffs[1] += 1
  end
end

puts diffs.reduce(&:*)
