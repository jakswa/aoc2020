class Runner
  @input = File.read('./day10input.txt').lines.map(&:to_i).sort
  @input.unshift(0)
  @max = @input.last + 3
  @input.push(@max)
  @paths = 0

  @nodes = {}
  @input.each do |i|
    @nodes[i] ||= []
    ((i-3)...i).each { |j| @nodes[j].push(i) if @nodes.key?(j) }
  end

  @chunks = []
  @max_seen = 0

  def self.explore(i)
    @max_seen = i if i > @max_seen
    return 1 if @nodes[i].length == 1
    sum = 0
    @nodes[i].each do |j|
      sum += explore(j)
    end
    sum
  end

  def self.dig(i)
    case @nodes[i].length
    when 1 then dig(@nodes[i][0])
    when 0 then return @chunks.reduce(&:*)
    else
      @chunks.push(@nodes[i].reduce(0) { |sum, j| sum + explore(j) })
      dig(@max_seen)
    end
  end
end

puts Runner.dig(0)

