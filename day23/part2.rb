input = '418976235'.split('').map(&:to_i)
# input = '389125467'.split('').map(&:to_i)
# input = '123456789'.split('').map(&:to_i)

len = 1_000_000
llist = {}
(0...input.length).each do |i|
  llist[input[i]] = [ input[i-1], input[i+1] ]
end
llist[input.first][0] = len
llist[len] = [len-1, input.first]
llist[input.last][1] = input.max + 1
llist[input.max + 1] = [input.last, input.max + 2]


nxt = ->(label) { llist.dig(label, 1) || label + 1 }
prev = ->(label) { llist.dig(label, 0) || label - 1 }
hooknxt = -> (label, targ) {
  llist[label] ||= [label - 1, label + 1]
  llist[label][1] = targ
}
hookprev = -> (label, targ) {
  llist[label] ||= [label - 1, label + 1]
  llist[label][0] = targ
}
splice = -> (left, right, at) {
  after = llist.dig(at, 1) || at + 1
  hooknxt.call(at, left)
  hookprev.call(left, at)
  hooknxt.call(right, after)
  hookprev.call(after, right)
}
unsplice = -> (l, r) {
  left = llist.dig(l, 0) || l - 1
  right = llist.dig(r, 1) || r + 1
  hooknxt.call(left, right)
  hookprev.call(right, left)
}
shift = ->(label) {
  rem = llist.delete(label) || [label-1, label+1]
  hooknxt.call(rem[0], rem[1])
  hookprev.call(rem[1], rem[0])
}
unshift = ->(left, targ) {
  after = llist.dig(left, 1) || left + 1
  hooknxt.call(left, targ)
  llist[targ] = [left, after]
  hookprev.call(after, targ)
}
vals = ->(val) {
  ret = val.to_s
  ret << ",#{val}" while val = llist.dig(val, 1)
  ret
}

start = input.first
curr = 1_000_000
10_000_000.times do
  curr = nxt.call(curr)
  left = nxt.call(curr)
  mid = nxt.call(left)
  right = nxt.call(mid)
  unsplice.call(left, right)
  splicelab = (curr - 2) % len + 1
  splicelab = (splicelab - 2) % len + 1 while (splicelab == left || splicelab == mid || splicelab == right)
  splice.call(left, right, splicelab)
end

first = nxt.call(1)
second = nxt.call(first)
puts "hoo: #{first} * #{second} = #{first * second}"
