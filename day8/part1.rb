input = File.read("./day8input.txt").lines.to_a
  .map { |line| line.split(' ') }
  .each { |line| line[1] = line[1].to_i }

acc = 0
curr = 0
ran = {}

loop do
  break if ran.key?(curr)
  ran[curr] = true
  instr, arg = input[curr]
  case instr
  when 'acc' then acc += arg
  when 'jmp' then curr += (arg - 1)
  end
  curr += 1
end

puts "acc: #{acc}"
