class Runner
  def self.acc
    @acc
  end

  @toggles = %w[nop jmp]
  @input = File.read("./day8input.txt").lines.to_a
    .map { |line| line.split(' ') }
    .each { |line| line[1] = line[1].to_i }

  def self.toggle_next
    if last = @toggle && @input[@toggle]&.first
      @input[@toggle][0] = last == 'nop' ? 'jmp' : 'nop'
    end
    @toggle ||= -1
    @toggle += 1
    @toggle += 1 until @toggle >= @input.length || @toggles.include?(@input[@toggle][0])
    raise "no toggle left" if @toggle >= @input.length
    @input[@toggle][0] = @input[@toggle][0] == 'nop' ? 'jmp' : 'nop'
  end

  def self.dupe_instr
    toggle_next
    reset
  end

  def self.run
    reset
    loop do
      dupe_instr if @ran.key?(@curr)
      @ran[@curr] = true
      instr, arg = @input[@curr]
      case instr
      when 'acc' then @acc += arg
      when 'jmp' then @curr += (arg - 1)
      end
      @curr += 1
      break if @curr >= @input.length
    end
  end

  def self.reset
    @acc = 0
    @curr = 0
    @ran = {}
  end
end

Runner.run
puts "acc: #{Runner.acc}"
