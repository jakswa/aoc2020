# input = '((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2'.gsub(' ', '')
input = File.read('./day18input.txt').gsub(' ', '')
math = input.lines.map(&:strip)

results = []
ops = []
output = []
math.each do |expr|
  output = []
  ops = []
  expr.chars.each do |char|
    case char
    when '*'
      output.push(ops.pop) until ops.empty? || ops.last == '('
      # output.push(ops.pop) until ops.empty? || ['+', '('].include?(ops.last)
      ops.push(char)
    when '+' then
      ops.push(char)
    when /\d/ then output.push(char.to_i)
    when '(' then ops.push(char)
    when ')'
      output.push(ops.pop) until ops.last == '('
      ops.pop
    end
    # puts "for #{char}, #{output.inspect} - #{ops.inspect}"
  end
  output.push(ops.pop) while ops.length > 0
  # puts "stack: #{output}"
  result = output.each_with_object([]) do |char, stack|
    if ['+', '*'].include?(char)
      stack.push stack.pop.send(char, stack.pop)
    else
      stack.push char
    end
  end.last
  puts "stack: #{result}"
  results.push(result)
end

puts "results: #{results.reduce(&:+)}"
