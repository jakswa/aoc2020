input = File.read('./day18input.txt').gsub(' ', '')
math = input.lines.map(&:strip)

results = []
math.each do |expr|
  ops = []
  vals = []
  expr.chars.each do |char|
    # puts "for #{char}, vals: #{vals.inspect}\nops: #{ops.inspect}"
    case char
    when '+', '*', '-' then ops.push(char)
    when /\d/
      if vals.length > 0 && vals.last != '('
        vals.push(vals.pop.send(ops.pop, char.to_i))
      else
        vals.push(char.to_i)
      end
    when '('
      vals.push(char)
    when ')'
      val = vals.pop
      raise unless vals.last == '('
      vals.pop
      if vals.length > 0 && vals.last.is_a?(Integer)
        vals.push(vals.pop.to_i.send(ops.pop, val.to_i))
      else
        vals.push val
      end
    end
  end
  raise unless vals.length == 1
  results.push(vals.last)
end

puts "results: #{results.reduce(&:+)}"
