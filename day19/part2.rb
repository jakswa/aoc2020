input = File.read('./day19input.txt').lines.map(&:strip)

rules = {}
msgs = []
input.each do |line|
  next if line == ""
  if line[/\d: /]
    id, rule = line.split(': ')
    if rule.start_with?('"')
      rules[id.to_i] = rule.gsub('"', '')
    else
      rules[id.to_i] = rule.split(' | ').map {|i| i.split(' ').map(&:to_i) }
    end
  else
    msgs.push(line)
  end
end

# puts "rules: #{rules}"
# puts "--8: #{rules[8]}"
rules[8] = [[42, '+']]
rules[11] = [['(?<fuck>', 42, '\g<fuck>?', 31, ')']]

def dig(id, rules)
  return id if id.is_a?(String)
  return rules[id] if rules[id].is_a?(String)
  rules[id].map do |rule|
    dug = rule.map {|rid| dig(rid, rules) }
    if dug.all?(String)
      dug.join('') 
    elsif dug.all?(Array)
      dug.map {|jj| "(#{jj.join('|')})" }.join('')
    else
      dug.map do |top|
        if top.is_a?(String)
          top
        else
          "(#{top.join("|")})"
        end
      end.join('')
    end
  end
end

# puts dig(0, rules)
regexp = Regexp.new("^#{dig(0,rules)[0]}$")
puts msgs.count { |msg| regexp.match?(msg) }
