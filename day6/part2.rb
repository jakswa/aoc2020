input = File.read('./day6input.txt')

sum = 0
grps = []
input.lines.each do |line|
  if line == "\n"
    sum += grps.reduce(&:&).length
    grps = []
    next
  end

  grps.push line.strip.split('')
end
sum += grps.reduce(&:&).length

puts "total: #{sum}"
