input = File.read('./day6input.txt')

sum = 0
grp = []
input.lines.each do |line|
  if line == "\n"
    sum += grp.uniq.length
    grp = []
    next 
  end

  grp += line.strip.split('')
end
sum += grp.uniq.length

puts "total: #{sum}"
