input = File.read('./day13input.txt').lines.to_a
busses = input[1].split(',').map.with_index do |i, ind|
  [i.to_i, ind] if i != 'x'
end.compact
maxbus = busses.max_by {|bus| bus[0]}
busses.delete(maxbus)

ts = 0 - maxbus[1]
incr = maxbus[0]
loop do
  ts += incr
  busses.delete_if do |bus|
    tss = ts + bus[1]
    hit = tss % bus[0] == 0
    incr = incr.lcm(bus[0]) if hit
    hit
  end
  break if busses.empty?
end

puts "done: #{ts}"
