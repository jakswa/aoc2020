input = File.open('./day24input.txt', 'r')
# input = File.open('./example.txt', 'r')

grid = {}
input.each_line do |line|
  coord = [0,0]
  skip = false
  (0...line.length).each do |i|
    if skip
      skip = false
      next
    end
    char = line.slice(i,2)
    case char
    when 'se'
      coord[0] += 1
      skip = true
    when 'sw'
      coord[0] += 1
      coord[1] -= 1
      skip = true
    when 'nw'
      coord[0] -= 1
      skip = true
    when 'ne'
      coord[0] -= 1
      coord[1] += 1
      skip = true
    when /^e/
      coord[1] += 1
    when /^w/
      coord[1] -= 1
    end
  end
  if grid.key?(coord)
    grid.delete(coord)
  else
    grid[coord] = true # black
  end
end

ADJ = [[0,-1],[0,1],[-1,0],[1,0],[1,-1],[-1,1]]
def neighbors(grid, row, col)
  ADJ.count do |dr,dc|
    nr = row + dr
    nc = col + dc
    black = grid.key?([nr, nc])
    yield(nr, nc) if block_given? && !black
    black
  end
end
100.times do
  flip = {}
  grid.each do |(row,col), _tru|
    blacks = neighbors(grid, row, col) do |white_row, white_col|
      nblack = neighbors(grid, white_row, white_col)
      flip[[white_row, white_col]] = true if nblack == 2
    end
    if blacks.zero? || blacks > 2
      flip[[row, col]] = true
    end
  end
  flip.keys.each do |coord|
    if grid.key?(coord)
      grid.delete(coord)
    else
      grid[coord] = true
    end
  end
end

puts "hoo: #{grid.length}"
