input = File.open('./day24input.txt', 'r')

grid = {}
input.each_line do |line|
  coord = [0,0]
  skip = false
  (0...line.length).each do |i|
    if skip
      skip = false
      next
    end
    char = line.slice(i,2)
    case char
    when 'se'
      coord[0] += 1
      skip = true
    when 'sw'
      coord[0] += 1
      coord[1] -= 1
      skip = true
    when 'nw'
      coord[0] -= 1
      skip = true
    when 'ne'
      coord[0] -= 1
      coord[1] += 1
      skip = true
    when /^e/
      coord[1] += 1
    when /^w/
      coord[1] -= 1
    end
  end
  if grid.key?(coord)
    grid.delete(coord)
  else
    grid[coord] = true # black
  end
end

puts "hmm #{grid.length}"
