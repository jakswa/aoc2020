input = File.read("./day9input.txt").lines.map(&:to_i)

curr = 0
inval = 375054920

loop do
  ((curr+1)...input.length).each do |edge|
    if input[curr..edge].sum == inval
      puts input[curr..edge].min + input[curr..edge].max
      raise "done"
    end
  end
  curr += 1
  break if curr >= input.length
end
