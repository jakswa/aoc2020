input = File.read("./day9input.txt").lines.map(&:to_i)
preamble_len = 25

curr = preamble_len + 1

loop do
  throw input[curr] unless ((curr - preamble_len)...curr).to_a.combination(2).find do |a,b|
    input[a] + input[b] == input[curr]
  end
  curr += 1
end
