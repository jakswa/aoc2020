input = File.open('./day21input.txt', 'r');

allerg = {}
ingreds = []
input.each_line do |line|
  ingred, allergens = line.strip[0...-1].split(" (contains ")
  ingred = ingred.split(' ').sort
  ingreds.concat(ingred)
  allergens.split(/,? /).each do |allergen|
    allerg[allergen] ||= []
    allerg[allergen].push ingred
    allerg[allergen].uniq!
  end
end

allergmap = {}
loop do
  startlen = allergmap.length
  allerg.each do |alle, ingred_lists|
    common = ingred_lists.reduce(&:&) - allergmap.values
    allergmap[alle] = common.first if common.length == 1
  end
  break if allergmap.length == startlen
end

puts allergmap.to_a.sort_by {|i| i[0] }.map {|i| i [1]}.join(',')
