input = File.read("./day2input.txt")

lines = input.lines.map { |l| l.split(/[- :]+/) }

valid = lines.select do |min, max, lett, pw|
  cnt = pw.scan(lett).length
  cnt >= min.to_i && cnt <= max.to_i
end

puts valid.length
