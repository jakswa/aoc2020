input = File.read("./day2input.txt")

lines = input.lines.map { |l| l.split(/[- :]+/) }

valid = lines.select do |min, max, lett, pw|
  (pw[min.to_i - 1] == lett) ^ (pw[max.to_i - 1] == lett)
end

puts valid.length
