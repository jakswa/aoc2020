rows = File.read('./day11input.txt').lines.to_a.map { |row| row.strip.split('') }
SLIDER = [-1,0,1].freeze

def adj(row, col, rows)
  cnt = 0
  SLIDER.each do |x|
    SLIDER.each do |y|
      next if x == 0 && y == 0
      newy = col+y
      newx = row+x
      next if newy < 0 || newx >= rows.length
      next if newx < 0 || newy >= rows[0].length
      cnt += 1 if rows[newx][newy] == '#'
    end
  end
  cnt
end

loop do
  changed = false
  prevrows = rows.map(&:dup)
  rows.each_with_index do |row, rind|
    row.each_with_index do |val, cind|
      occupied = adj(rind, cind, prevrows)
      if val == '#' && occupied >= 4
        rows[rind][cind] = 'L'
        changed = true
      elsif val == 'L' && occupied == 0
        rows[rind][cind] = '#'
        changed = true
      end
    end
  end
  break unless changed
  # puts "----\n #{rows.map {|r| r.join('') }.join("\n")}\n"
end

cnt = 0
rows.each {|row| row.each {|val| cnt += 1 if val == '#' }}
puts "cnt: #{cnt}"
