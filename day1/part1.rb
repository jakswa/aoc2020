input = File.read('./input.txt')

match = input.lines.map(&:to_i).combination(2).find { |i,j| i + j == 2020 }

puts "Match: #{match}"
puts "result: #{match.first * match.last}"
