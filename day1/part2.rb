input = File.read('./input.txt')

match = input.lines.map(&:to_i).combination(3)
  .find { |i, j, k| i + j + k == 2020 }

puts "Match: #{match}"
puts "result: #{match[0] * match[1] * match[2]}"
