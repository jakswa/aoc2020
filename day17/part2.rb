# input = ".#.\n..#\n###".lines.map(&:strip)
input = File.read('./day17input.txt').lines.map(&:strip)

WID = 40
grid = Array.new(WID) { Array.new(WID) { Array.new(WID) { Array.new(WID) }}}

input.each_with_index do |line, yind|
  line.chars.each_with_index do |char, zind|
    next unless char == '#'
    grid[WID/2][WID/2][WID/2 + yind][WID/2 + zind] = true
  end
end


def neighbors(x,y,z,w,grid)
  cnt = 0
  (-1..1).each do |xx|
    (-1..1).each do |yy|
      (-1..1).each do |zz|
        (-1..1).each do |ww|
          next if xx == 0 && yy == 0 && zz == 0 && ww == 0
          nx = x + xx
          ny = y + yy
          nz = z + zz
          nw = w + ww
          next if nx < 0 || ny < 0 || nz < 0 || nw < 0
          next if nx >= WID || ny >= WID || nz >= WID || nw >= WID
          cnt += 1 if grid.dig(nx,ny,nz,nw)
        end
      end
    end
  end
  cnt
end

6.times do |time|
  changes = []
  grid.each_with_index do |yarr, x|
    yarr.each_with_index do |zarr, y|
      zarr.each_with_index do |warr, z|
        warr.each_with_index do |active, w|
          actives = neighbors(x,y,z,w,grid)
          if active && !actives.between?(2,3)
            changes.push([x,y,z,w,false])
          elsif !active && actives == 3
            changes.push([x,y,z,w,true])
          end
        end
      end
    end
  end
  changes.each {|(x,y,z,w,active)| grid[x][y][z][w] = active }
  puts "tick #{time}"
end

puts "wat: #{grid.flatten.count {|i| i}}"
