input = File.read('./day16input.txt').lines.to_a

fields = {}
until input[0] == "\n"
  name, val = input[0].split(': ')
  fields[name] = val.split(' or ').map {|i| i.split('-').map(&:to_i) }
  input.shift
end
input.shift(2)
my_ticket = input[0].split(',').map(&:to_i)
input.shift(3)
nearby_tickets = input.map {|line| line.split(',').map(&:to_i) }

valid_tickets = nearby_tickets.select do |ticket|
  ticket.all? { |val| fields.values.find { |ranges| ranges.find { |min, max| val.between?(min, max) }}}
end

inds = (0...valid_tickets[0].length).to_a

fieldmap = {}
while fieldmap.length < fields.length
  fields.each do |key, ranges|
    vinds = inds.select do |ind|
      hmm = valid_tickets.select { |ticket| ranges.find { |min, max| ticket[ind].between?(min, max) }}
      hmm.length == valid_tickets.length
    end
    if vinds.length == 1
      fieldmap[key] = vinds.first 
      inds.delete(vinds.first)
    end
  end
end

done = []
fields.each do |key,_ranges|
  done.push(my_ticket[fieldmap[key]]) if key.start_with?('departure')
end
puts "done: #{done.reduce(&:*)}"
