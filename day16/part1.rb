input = File.read('./day16input.txt').lines.to_a

fields = {}
until input[0] == "\n"
  name, val = input[0].split(': ')
  fields[name] = val.split(' or ').map {|i| i.split('-').map(&:to_i) }
  input.shift
end
input.shift(2)
my_ticket = input[0].split(',').map(&:to_i)
input.shift(2)
nearby_tickets = input.map {|line| line.split(',').map(&:to_i) }

invalid_vals = []
nearby_tickets.map do |ticket|
  invalid_vals.push ticket.select { |val| !fields.values.find { |ranges| ranges.find { |min, max| val.between?(min, max) }}}
end

puts "done: #{invalid_vals}"
