input = File.read('./day5input.txt')

seating = input.lines.map do |line|
  row = 128 / 2
  rowsize = row
  seat = 4
  seatsize = seat
  7.times { |i| rowsize /= 2; line[i] == 'F' ? row -= rowsize : row += rowsize }
  3.times { |i| seatsize /= 2.0; line[i + 7] == 'R' ? seat += seatsize : seat -= seatsize }
  [row - 1, seat.floor]
end

puts "max: #{seating.map {|row, seat| row * 8 + seat }.max}"
