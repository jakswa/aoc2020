input = File.read('./day5input.txt')

seating = input.lines.map do |line|
  row = 128 / 2
  rowsize = row
  seat = 4
  seatsize = seat
  7.times { |i| rowsize /= 2.0; line[i] == 'F' ? row -= rowsize : row += rowsize }
  3.times { |i| seatsize /= 2.0; line[i + 7] == 'R' ? seat += seatsize : seat -= seatsize }
  (row.floor) * 8 + seat.floor
end.sort

seating = seating.map {|i| i - 32 }

puts "found: #{seating.find.with_index {|i,ind| i != ind } + 31 }"
