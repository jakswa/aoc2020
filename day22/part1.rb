input = File.open('./day22input.txt', 'r')

decks = [[], []]
deck_ind = 0
input.each_line do |line|
  if line == "\n"
    deck_ind += 1
  elsif line[/^\d/]
    decks[deck_ind].push line.to_i
  end
end

def play(decks)
  while decks.none?(&:empty?)
    p1 = decks[0].shift
    p2 = decks[1].shift

    if p1 > p2
      decks[0].push(p1)
      decks[0].push(p2)
    else
      decks[1].push(p2)
      decks[1].push(p1)
    end
  end
  decks
end
play(decks)

winner = decks.flatten
mult = 0
score = 0
score += winner.pop * (mult += 1) until winner.empty?
puts "score: #{score}"
