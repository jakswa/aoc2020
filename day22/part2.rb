input = File.open('./day22input.txt', 'r')

decks = [[], []]
deck_ind = 0
input.each_line do |line|
  if line == "\n"
    deck_ind += 1
  elsif line[/^\d/]
    decks[deck_ind].push line.to_i
  end
end


def play(decks)
  round_decks = []
  while decks.none?(&:empty?)
    return [[1],[]] if round_decks.include?(decks)
    round_decks.push(decks.map(&:clone))

    p1 = decks[0].shift
    p2 = decks[1].shift
    # puts "-/ #{p1} : #{decks[0].join(',')}"
    # puts "-\\ #{p2} : #{decks[1].join(',')}"

    if decks[0].length >= p1 && decks[1].length >= p2
      # puts "dive: #{p1},#{p2}"
      res = play([decks[0].slice(0,p1), decks[1].slice(0,p2)])
      p1win = res[1].empty?
    else
      p1win = p1 > p2
    end

    if p1win
      decks[0].push(p1)
      decks[0].push(p2)
    else
      decks[1].push(p2)
      decks[1].push(p1)
    end
  end
  decks
end
winner = play(decks).flatten

mult = 0
score = 0
score += winner.pop * (mult += 1) until winner.empty?
puts "score: #{score}"
