input = File.read('./day20input.txt').lines.map(&:strip)

tiles = {}
sides = {}

id = nil
input.each do |line|
  next if line == ""
  if line.start_with?('Tile')
    id = line.split(' ')[1].to_i
  else
    tiles[id] ||= { id: id, lines: [], sides: [], conns: [], rot: 0 }
    tiles[id][:lines].push(line.chars.to_a)
  end
end

tiles.each do |id, obj|
  obj[:sides][0] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][0][ind]
  end
  obj[:sides][1] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].last
  end
  obj[:sides][2] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][-1][ind]
  end
  obj[:sides][3] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].first
  end
  obj[:sides].each do |side|
    keyline = [side, side.reverse].sort.first
    sides[keyline] ||= []
    sides[keyline].push obj[:id]
    sides[keyline].uniq!
  end
end

LEN = tiles.values.first[:lines].length
IDLEN = tiles.keys.length
GRIDLEN = Math.sqrt(IDLEN).to_i
SIDES = [[-GRIDLEN,0,2], [1,1,3], [GRIDLEN,2,0], [-1,3,1]]

def fliph(sides)
  tmp = sides[0]
  sides[0] = sides[2]
  sides[2] = tmp
  sides[1].reverse!
  sides[3].reverse!
end

def flipv(sides)
  tmp = sides[1]
  sides[1] = sides[3]
  sides[3] = tmp
  sides[0].reverse!
  sides[2].reverse!
end

map = {}
tiles.each do |id, tile|
  tile[:sides].each do |side|
    keyside = [side, side.reverse].sort.first
    conns = (sides[keyside] - [id])
    tile[:conns].push(conns.first)
  end
end

result = tiles.select do |id, tile|
  tile[:conns].compact.length == 2
end.keys.reduce(&:*)
puts "result: #{result}"
