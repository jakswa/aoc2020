input = File.read('./day20input.txt').lines.map(&:strip)

tiles = {}
sides = {}

id = nil
input.each do |line|
  next if line == ""
  if line.start_with?('Tile')
    id = line.split(' ')[1].to_i
  else
    tiles[id] ||= { id: id, lines: [], sides: [], conns: [], rot: 0 }
    tiles[id][:lines].push(line.chars.to_a)
  end
end

tiles.each do |id, obj|
  obj[:sides][0] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][0][ind]
  end
  obj[:sides][1] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].last
  end
  obj[:sides][2] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][-1][ind]
  end
  obj[:sides][3] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].first
  end
  obj[:sides].each do |side|
    keyline = [side, side.reverse].sort.first
    sides[keyline] ||= []
    sides[keyline].push obj[:id]
    sides[keyline].uniq!
  end
  obj[:lines] = obj[:lines][1...-1].map {|line| line[1...-1] }
end

tiles.each do |id, tile|
  tile[:sides].each do |side|
    keyside = [side, side.reverse].sort.first
    conns = (sides[keyside] - [id])
    tile[:conns].push(conns.first)
  end
end

topleft = tiles.values.find {|tile| tile[:conns][1] && tile[:conns][2] && !tile[:conns][0] && !tile[:conns][3]}
if !topleft
  topleft = tiles.values.find {|tile| tile[:conns][2] && tile[:conns][3] && !tile[:conns][0] && !tile[:conns][1]} 
  topleft[:rot] = 3 if topleft
  topleft[:sides].each(&:reverse!) if topleft
end
left = tile = topleft
gridrow = 0

grid_ids = []

loop do
  grid_ids.push(tile[:id])
  nxtile = tiles[tile[:conns][(1 - tile[:rot]) % 4]]
  if !nxtile
    tile = tiles[left[:conns][(2 - left[:rot]) % 4]]
    break unless tile
    side = tile[:sides].index {|s| !(left[:sides] & [s, s.reverse]).empty? }
    tile[:rot] = (0 - side) % 4
    if tile[:sides][side] != left[:sides][(2 - left[:rot]) % 4]
      tile[:rot] = (tile[:rot] + 2) % 4
      tile[:flip] = true
    end
    case tile[:rot]
    when 1
      tile[:sides][1].reverse!
      tile[:sides][3].reverse!
    when 2,3
      tile[:sides].each(&:reverse!)
    end
    left = tile
    grid_ids.push(tile[:id])
    nxtile = tiles[tile[:conns][(1 - tile[:rot]) % 4]]
  end
  side = nxtile[:sides].index {|s| !(tile[:sides] & [s, s.reverse]).empty? }
  nxtile[:rot] = 3 - side
  case nxtile[:rot]
  when 1
    nxtile[:sides][1].reverse!
    nxtile[:sides][3].reverse!
  when 2,3
    nxtile[:sides][0].reverse!
    nxtile[:sides][1].reverse!
    nxtile[:sides][2].reverse!
    nxtile[:sides][3].reverse!
  end
  nxtile[:flip] = nxtile[:sides][side] != tile[:sides][(1 - tile[:rot]) % 4]
  tile = nxtile
end

PICSIZE = tiles.values.first[:lines].length
PICLEN = PICSIZE - 1
raise 'wat' unless tiles.values.first[:lines].first.length == PICLEN + 1
def rowcol(row,col,tile)
  case tile[:rot]
  when 0
    rrow = row
    rrow = PICLEN - rrow if tile[:flip]
    tile[:lines][rrow][col]
  when 1
    rrow = PICLEN - col
    rrow = PICLEN - rrow if tile[:flip]
    tile[:lines][rrow][row]
  when 2
    rrow = PICLEN - row
    rrow = PICLEN - rrow if tile[:flip]
    tile[:lines][rrow][PICLEN-col]
  when 3
    rrow = PICLEN - col
    rrow = PICLEN - rrow if tile[:flip]
    tile[:lines][rrow][PICLEN-row]
  end
end

lines = []
GRIDSIZE = Math.sqrt(grid_ids.length).to_i
grid_ids.each.with_index do |tile_id, grid_pos|
  t = tiles[tile_id]
  xstart = (grid_pos / GRIDSIZE) * PICSIZE
  ystart = (grid_pos % GRIDSIZE) * PICSIZE
  (0..PICLEN).each do |x|
    (0..PICLEN).each do |y|
      lines[xstart + x] ||= []
      lines[xstart + x][ystart + y] = rowcol(x,y,t)
    end
  end
end

monster = <<-STR.gsub(' ', '.').lines.map(&:strip).map(&:chars).map(&:to_a)
                  # 
#    ##    ##    ###
 #  #  #  #  #  #
STR

monster_inds = []
monster.each_with_index do |row, rind|
  row.each_with_index do |val, cind|
    monster_inds.push([monster.first.length - cind - 1, rind]) if val == '#'
  end
end

count = 0
(0..(lines.length - monster.first.length)).each do |row|
    (0..(lines[0].length - monster.length)).each do |col|
      count += 1 if monster_inds.all? do |(mx, my)|
        lines[row + mx][col + my] == '#'
      end
    end
  end
puts "done: #{count}"
