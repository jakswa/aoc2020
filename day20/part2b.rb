input = File.read('./example.txt').lines.map(&:strip)

tiles = {}
sides = {}

id = nil
input.each do |line| next if line == ""
  if line.start_with?('Tile')
    id = line.split(' ')[1].to_i
  else
    tiles[id] ||= { id: id, lines: [], sides: [], conns: [], rot: 0 }
    tiles[id][:lines].push(line.chars.to_a)
  end
end

tiles.each do |id, obj|
  obj[:sides][0] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][0][ind]
  end
  obj[:sides][1] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].last
  end
  obj[:sides][2] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][-1][ind]
  end
  obj[:sides][3] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].first
  end
  obj[:sides].each do |side|
    keyline = [side, side.reverse].sort.first
    sides[keyline] ||= []
    sides[keyline].push obj[:id]
    sides[keyline].uniq!
  end
  obj[:lines] = obj[:lines][1...-1].map {|line| line[1...-1] }
end

tiles.each do |id, tile|
  tile[:sides].each do |side|
    keyside = [side, side.reverse].sort.first
    conns = (sides[keyside] - [id])
    tile[:conns].push(conns.first)
  end
end

def rot(tile, num)
  raise "needs to take into account rotation"
  if num.between?(2,3)
    tile[:sides].each(&:reverse!)
  elsif num == 1
    tile[:sides][1].reverse!
    tile[:sides][3].reverse!
  end
  tile[:rot] = (tile[:rot] + num) % 4
end
def flipv(tile)
  raise "needs to take into account rotation"
  tile[:flipv] = !tile[:flipv]
  tile[:sides][1].reverse!
  tile[:sides][3].reverse!
  tmp = tile[:sides][0]
  tile[:sides][0] = tile[:sides][2]
  tile[:sides][2] = tmp
  tmp = tile[:conns][0]
  tile[:conns][0] = tile[:conns][2]
  tile[:conns][2] = tmp
end
def fliph(tile)
  rot(tile, 2)
  flipv(tile)
end
def side(tile, ind)
  tile[:sides][(ind - tile[:rot]) % 4]
end
def conn_id(tile, ind)
  tile[:conns][(ind - tile[:rot]) % 4]
end
def popnext(prev, tiles)
  nxtile = tiles[conn_id(prev, 1)]
  return nil unless nxtile
  rot(nxtile, 3 - nxtile[:conns].index(prev[:id]))
  flipv(nxtile) if side(prev, 1) != side(nxtile, 3)
  raise "hmm fishy" unless side(prev, 1) == side(nxtile, 3)
  nxtile
end
def popdown(above, tiles)
  nxtile = tiles[conn_id(above, 2)]
  return nil unless nxtile
  puts "hmmm: #{nxtile.slice(:id, :conns, :rot, :flipv)}, sides;\n#{nxtile[:sides].map {|i|i.join('')}.join("\n")}"
  rot(nxtile, (4 - nxtile[:conns].index(above[:id])) % 4)
  puts "hmmm: #{nxtile.slice(:id, :conns, :rot, :flipv)}, sides;\n#{nxtile[:sides].map {|i|i.join('')}.join("\n")}"
  fliph(nxtile) if side(above, 2) != side(nxtile, 0)
  puts "hmmm: #{nxtile.slice(:id, :conns, :rot, :flipv)}, sides;\n#{nxtile[:sides].map {|i|i.join('')}.join("\n")}"
  raise "hmm fishy, #{side(above, 2)} != #{side(nxtile, 0)}" unless side(above, 2) == side(nxtile, 0)
  nxtile
end

tile = left = tiles.values.find {|tile| tile[:conns].compact.length == 2 }
case tile[:conns].slice(0,2).map {|i| !i.nil? }
when [true, true] then rot(tile, 1)
when [true, false] then rot(tile, 2)
when [false, false] then rot(tile, 3)
end

while left
  tile = popnext(tile, tiles) while tile
  left = popdown(left, tiles)
  break unless left
  tile = popnext(left, tiles)
end

puts "whew. it good."
