input = File.read('./day20input.txt').lines.map(&:strip)

tiles = {}
sides = {}

id = nil
input.each do |line|
  next if line == ""
  if line.start_with?('Tile')
    id = line.split(' ')[1].to_i
  else
    tiles[id] ||= { id: id, lines: [], sides: [], conns: [], rot: 0 }
    tiles[id][:lines].push(line.chars.to_a)
  end
end

tiles.each do |id, obj|
  obj[:sides][0] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][0][ind]
  end
  obj[:sides][1] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].last
  end
  obj[:sides][2] = (0...obj.dig(:lines, 0).length).map do |ind|
    obj[:lines][-1][ind]
  end
  obj[:sides][3] = (0...obj.dig(:lines).length).map do |ind|
    obj[:lines][ind].first
  end
  obj[:sides].each do |side|
    keyline = [side, side.reverse].sort.first
    sides[keyline] ||= []
    sides[keyline].push obj[:id]
    sides[keyline].uniq!
  end
end

tiles.each do |id, tile|
  tile[:sides].each do |side|
    keyside = [side, side.reverse].sort.first
    conns = (sides[keyside] - [id])
    tile[:conns].push(conns.first)
  end
end

def printed(tile)
  tile[:lines].map {|i| i.join('')}.join("\n")
end
def pcompare(tiles)
  lines = tiles.map {|i| i[:lines] }
  nll = lines.first.length
  (0...nll).map do |row|
    lines[0][row].join('') + '|' + lines[1][row].join('')
  end.join("\n") + "\n------\n"
end

def fliph(tile)
  lines = tile[:lines]
  newlines = []
  (0...lines.length).each do |row|
    newline = []
    nll = lines[row].length
    (0...nll).each do |col|
      newline.push(lines[row][nll - col - 1])
    end
    newlines.push newline
  end
  tile[:lines] = newlines
  tmp = tile[:conns][1]
  tile[:conns][1] = tile[:conns][3]
  tile[:conns][3] = tmp
end
def flipv(tile)
  lines = tile[:lines]
  newlines = []
  (0...lines.length).each do |row|
    newline = []
    (0...lines[row].length).each do |col|
      newline.push(lines[lines.length - row - 1][col])
    end
    newlines.push newline
  end
  tile[:lines] = newlines
  tmp = tile[:conns][0]
  tile[:conns][0] = tile[:conns][2]
  tile[:conns][2] = tmp
end
def rot(tile)
  lines = tile[:lines]
  newlines = []
  nll = lines.length
  (0...nll).each do |row|
    newline = []
    (0...nll).each do |col|
      newline.push(lines[nll - col - 1][row])
    end
    newlines.push newline
  end
  tile[:lines] = newlines
  tile[:conns].unshift tile[:conns].pop
end
def matchv?(tile, target)
  llines = ltile[:lines]
  rlines = rtile[:lines]
  len = llines.length
  (0...len).each.all? do |i|
    llines[i].last == rlines[i].first
  end
end
def matchvals(tile, ind1, val1, ind2, val2)
  rot(tile) until tile[:conns][ind1] == val1
  return if tile[:conns][ind2] == val2
  if ind2 == 0 || ind2 == 2
    flipv(tile)
  else
    fliph(tile)
  end
  return if tile[:conns][ind2] == val2
  rot(tile)
  flipv(tile)
end

gridlen = Math.sqrt(tiles.length).round
tile = left = tiles.values.find {|tile| tile[:conns].count {|i| i.nil?} == 2 }
matchvals(tile, 0, nil, 3, nil)
target = tiles[tile[:conns][1]]
top = nil
grid = [[tile]]
gridlen.times do |r|
  gridlen.times do |i|
    break unless target
    matchvals(target, 0, top && top[:id], 3, tile[:id])
    grid.last.push target
    tile = target
    target = tiles[tile[:conns][1]]
    top = tiles[top[:conns][1]] if top
  end
  top = left
  tile = left = tiles[left[:conns][2]]
  break unless tile
  matchvals(tile, 0, top[:id], 3, nil)
  grid.push [tile]
  target = tiles[tile[:conns][1]]
  top = tiles[top[:conns][1]]
end

llen = grid[0][0][:lines].length
chargrid = [[]]
grid.each do |gridrow|
  (llen-2).times do |row|
    gridrow.each do |tile|
      tile[:lines][row+1][1...-1].each {|char| chargrid.last.push(char)}
    end
    chargrid.push []
  end
end

seamonster = (<<-STR).lines.map {|i| i.chomp.split('') }
                  # 
#    ##    ##    ###
 #  #  #  #  #  #
STR

rotmonster = ->() {
  newmonster = Array.new(seamonster[0].length) { [] }
  (0...newmonster.length).each do |nri|
    (0...seamonster.length).each do |nci|
      newmonster[nri][nci] = seamonster[seamonster.length - nci - 1][nri]
    end
  end
  seamonster = newmonster
}
flipmonster = ->() {
  newmonster = Array.new(seamonster.length) { [] }
  seamonster.each_with_index do |row, rowind|
    row.each_with_index do |char, colind|
      newmonster[seamonster.length - rowind - 1][colind] = char
    end
  end
  seamonster = newmonster
}

maxind = chargrid.length-1
matches = ->(rowst, colst) {
  mtiles = {}
  seamonster.each_with_index do |srow, srind|
    srow.each_with_index do |sval, scind|
      next unless sval == '#'
      grind = rowst + srind
      gcind = colst + scind
      return {} unless grind.between?(0, maxind)
      return {} unless gcind.between?(0, maxind)
      mtiles[[grind, gcind]] = true if chargrid[grind][gcind] == '#'
    end
  end
  return {} unless mtiles.length == 15
  mtiles
}

monstertiles = -> () {
  mtiles = {}
  (0...chargrid.length).each do |rind|
    row = chargrid[rind]
    (0...row.length).each do |cind|
      char = row[cind]
      m = matches.call(rind, cind)
      mtiles.merge!(m)
    end
  end
  mtiles
}

cnt = 0
finaltiles = nil
loop do
  finaltiles = monstertiles.call()
  break unless finaltiles.empty?
  rotmonster.call()
  flipmonster.call() if cnt % 3 == 2
  cnt += 1
end

total = chargrid.map {|i| i.count { |j| j == '#'}}.sum
puts "done: #{total} - #{finaltiles.length} = #{total - finaltiles.length}"
