input = File.read('./day14input.txt').lines.to_a


mask, mor, mand = nil
store = {}
input.each do |line|
  if line.start_with?('mask')
    mask = line.split(' = ')[1]
    mor = mask.gsub('X', '0').to_i(2)
    zeroind = mask.index('0')
    mand = mask.gsub('X', '1').to_i(2)
  else
    ind = line[/\d+\]/].to_i
    num = line[/\d+$/].to_i
    store[ind] = (num | mor) & mand
  end
end

puts store.values.sum
