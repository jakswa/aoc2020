input = File.read('./day14input.txt').lines.to_a

mask, mor, xcnt, xmaska, xmaskb = nil
store = {}
input.each do |line|
  if line.start_with?('mask')
    mask = line.split(' = ')[1].strip
    mor = mask.gsub('X', '0').to_i(2)
    xcnt = mask.count('X')
    xmaska = mask.gsub('1', '0')
    xmaskb = mask.gsub('0', '1')
  else
    ind = line[/\d+\]/].to_i | mor
    num = line[/\d+$/].to_i
    (0...(1 << xcnt)).each do |xind|
      xxmaska = xmaska.dup
      xxmaskb = xmaskb.dup
      xrep = xind.to_s(2).rjust(xcnt, '0')
      xrep.chars.each do |char|
        xxmaska.sub!('X', char)
        xxmaskb.sub!('X', char)
      end
      currind = (ind | xxmaska.to_i(2)) & xxmaskb.to_i(2)
      store[currind] = num
    end
  end
end

# puts store.inspect
puts store.values.sum
