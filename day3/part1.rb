input = File.read("./day3input.txt").lines.to_a.map(&:strip)

wid = input.first.length
pos = [0,0]
slope = [1,2]
trees = 0

while pos[1] < input.length
  trees += 1 if input[pos[1]][pos[0] % wid] == '#'
  pos[0] += slope[0]
  pos[1] += slope[1]
end

puts "hit #{trees} trees"

# ran it multiple times on other slopes, cataloged results
puts "p2: #{216 * 79 * 91 * 96 * 45}"
