input = '2,1,10,11,0,6'.split(',').map(&:to_i)

cnt = 0
last_spoken = {}
last = nil
turn = 0

input.each do |num|
  last_spoken[num] = [turn, turn]
  last = num
  turn += 1
end

loop do
  raise "last: #{last}" if turn == 30_000_000
  curr = last_spoken[last][1] - last_spoken[last][0]
  if last_spoken.key?(curr)
    arr = last_spoken[curr]
    arr.shift
    arr.push(turn)
  else
    last_spoken[curr] = [turn, turn]
  end
  last = curr
  turn += 1
end
