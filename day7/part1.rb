input = File.read("./day7input.txt")

map = {}
input.lines.each do |line|
  key = line[/^\w+ \w+/]
  map[key] ||= {}
  line.split(',').map { |i| i[/\d \w+ \w+/] }.each do |content|
    next unless content
    map[key][content[2..-1]] = content[0].to_i
  end
end

def dig(val, map)
  val.each do |ckey, _val|
    return true if ckey == 'shiny gold' || dig(map[ckey], map)
  end
  false
end

cnt = 0
map.each do |key, val|
  cnt += 1 if dig(val, map)
end

puts cnt
