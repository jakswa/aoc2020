input = File.read("./day7input.txt")

map = {}
input.lines.each do |line|
  key = line[/^\w+ \w+/]
  map[key] ||= {}
  line.split(',').map { |i| i[/\d \w+ \w+/] }.each do |content|
    next unless content
    map[key][content[2..-1]] = content[0].to_i
  end
end

def dig(val, map)
  return 1 unless val.length > 0
  cnt = 1
  val.each do |key, ccnt|
    cnt += ccnt * dig(map[key], map)
  end
  cnt
end

puts dig(map['shiny gold'], map) - 1
