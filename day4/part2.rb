req = %w[byr iyr eyr hgt hcl ecl pid]
EYES = %w[amb blu brn gry grn hzl oth]
input = File.read("./input.txt")

def validate(key, val)
  case key
  when 'byr' then val.to_i.between?(1920, 2002)
  when 'iyr' then val.to_i.between?(2010, 2020)
  when 'eyr' then val.to_i.between?(2020, 2030)
  when 'hgt' then height(val)
  when 'hcl' then val[/^#[a-f0-9]{6}$/]
  when 'ecl' then EYES.include?(val)
  when 'pid' then val[/^\d{9}$/]
  end
end

def height(val)
  if val.end_with?('cm')
    val[/^\d+/]&.to_i&.between?(150, 193)
  elsif val.end_with?('in')
    val[/^\d+/]&.to_i&.between?(59, 76)
  end
end

miss = req.clone
valid = 0
input.lines.each do |line|
  if line == "\n"
    valid += 1 if miss.empty?
    miss = req.clone
    next
  end

  line.split(" ").each do |kv|
    k,v = kv.split(':')
    miss.delete(k) if validate(k, v)
  end
end
valid += 1 if miss.empty?

puts "valid: #{valid}"
