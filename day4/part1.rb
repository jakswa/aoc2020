req = %w[byr iyr eyr hgt hcl ecl pid]
input = File.read("./input.txt")

miss = req.clone
valid = 0
input.lines.each do |line|
  if line == "\n"
    valid += 1 if miss.empty?
    miss = req.clone
    next
  end

  req.each { |key| miss.delete(key) if line.include?(key) }
end
valid += 1 if miss.empty?

puts "valid: #{valid}"
