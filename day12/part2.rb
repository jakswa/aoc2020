pos = [0,0]
dirmap = { 'E' => 0, 'S' => 1, 'W' => 2, 'N' => 3 }
dirs = [[1,0],[0,1],[-1,0],[0,-1]]
wp = [10,-1]
File.read('./day12input.txt').lines.each do |line|
  case line[0]
  when 'F'
    dist = line[1..-1].to_i
    wpp = [wp[0] - pos[0], wp[1] - pos[1]]
    pos[0] += wpp[0] * dist
    pos[1] += wpp[1] * dist
    wp = [pos[0] + wpp[0], pos[1] + wpp[1]]
  when 'R'
    wpp = [wp[0] - pos[0], wp[1] - pos[1]]
    oldrad = Math.atan2(wpp[1], wpp[0])
    rad = (line[1..-1].to_i / 180.0) * Math::PI
    newrad = oldrad + rad
    hyp = Math.sqrt(wpp[0] * wpp[0] + wpp[1] * wpp[1])
    wp = [pos[0] + Math.cos(newrad) * hyp, pos[1] + Math.sin(newrad) * hyp].map(&:round)
  when 'L'
    wpp = [wp[0] - pos[0], wp[1] - pos[1]]
    oldrad = Math.atan2(wpp[1], wpp[0])
    rad = (line[1..-1].to_i / 180.0) * Math::PI
    newrad = oldrad - rad
    hyp = Math.sqrt(wpp[0] * wpp[0] + wpp[1] * wpp[1])
    wp = [pos[0] + Math.cos(newrad) * hyp, pos[1] + Math.sin(newrad) * hyp].map(&:round)
  when /^[NSEW]/
    cdir = dirmap[line[0]]
    dist = line[1..-1].to_i
    wp[0] += dirs.dig(cdir,0) * dist
    wp[1] += dirs.dig(cdir,1) * dist
  end
  # puts "#{pos}, #{wp} after #{line}"
end
puts "puts #{pos}, dist: #{pos.map(&:abs).sum}"

