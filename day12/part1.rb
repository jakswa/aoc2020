dirmap = { 'E' => 0, 'S' => 1, 'W' => 2, 'N' => 3 }
pos = [0,0]
dirs = [[1,0],[0,1],[-1,0],[0,-1]]
dir = 0
File.read('./day12input.txt').lines.each do |line|
  case line[0]
  when 'F'
    dist = line[1..-1].to_i
    pos[0] += dirs.dig(dir % 4,0) * dist
    pos[1] += dirs.dig(dir % 4,1) * dist
  when 'R'
    deg = line[1..-1].to_i / 90
    dir += deg
  when 'L'
    deg = line[1..-1].to_i / 90
    dir -= deg
  when /^[NSEW]/
    cdir = dirmap[line[0]]
    dist = line[1..-1].to_i
    pos[0] += dirs.dig(cdir,0) * dist
    pos[1] += dirs.dig(cdir,1) * dist
  end
end
puts "dist: #{pos.sum}"

